[![License](https://www.gnu.org/graphics/gplv3-127x51.png)](https://gitlab.com/antoniotari/reactive-ampache/blob/master/LICENSE)


this README is still under construction, please be patient

- The app depends on other libraries , included as submodules.
To start just call:

```
git submodule init
git submodule update
```
- Java 8 minimum required 
<br>

- you need to have the following environment variable in your Bash profile with your own debug info :
```
AMPACHE_URL
AMPACHE_URL_LOCAL
AMPACHE_USERNAME
AMPACHE_PASSWORD
```
alternatively you can edit the build.gradle of the module `app` and put those build config variables to `null`, if you do so please do not commit that file.
<br>
- if you want to test your app with artist images and info you need a last.fm api key, defined in your Bash profile file as:
```
LASTFM_API_KEY
```
alternatively you can edit the build.gradle of the module `app` and put the `LASTFM_API_KEY` build config variable to `null` or to your api key, if you do so please do not commit that file.


<b>Contribute:</b><br>
to contribute please refer to the contribution guide.
please only work on issues from the issue tracker, if you find more issues please submit your issue and wait for approval.<br>
<b><i>name conventions:</i></b> the name of your branch should follow this pattern `PA-{issue number}-{name of branch}` , ie. `PA-6-visual_feedback_bug` <br>
your commit message should include the issue number and your name, follow this pattern `PA-{issue number} [{author}] {message}` , ie. `PA-6 [Antonio Tari] fixed thread blocking issue`