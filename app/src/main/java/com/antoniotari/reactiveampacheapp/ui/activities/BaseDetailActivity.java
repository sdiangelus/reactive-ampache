/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.antoniotari.android.lastfm.LastFmArtist;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.utils.AmpacheCache;
import com.antoniotari.reactiveampacheapp.utils.FastScrollWrapper;
import com.antoniotari.reactiveampacheapp.utils.Utils;

import xyz.danoz.recyclerviewfastscroller.sectionindicator.title.SectionTitleIndicator;
import xyz.danoz.recyclerviewfastscroller.vertical.VerticalRecyclerViewFastScroller;

/**
 * Created by antonio tari on 2016-05-29.
 */
public abstract class BaseDetailActivity extends BaseMusicActivity {

    protected ImageView mToolbarImageView;
    protected ImageView mBackgroundImage;
    protected Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbar;
    protected RecyclerView recyclerView;
    protected SwipeRefreshLayout swipeLayout;
    protected ProgressBar progressWait;

    protected LastFmArtist mLastFmArtist;

    protected PopupMenu mPopupMenu;

    protected FastScrollWrapper mFastScrollWrapper;

    protected abstract void initOnCreate();
    protected abstract LayoutManager getRecyclerViewLayoutManager();
    protected abstract void onRefresh();
    //protected abstract <T extends AmpacheModel> void initAdapter(List<T> items);

    protected void setDetailName(String name) {
        collapsingToolbar.setTitle(Utils.fromHtml(name));
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_artist;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mToolbarImageView = (ImageView) findViewById(R.id.toolbarImageView);
        mBackgroundImage = (ImageView) findViewById(R.id.backroundImage);
        progressWait = (ProgressBar) findViewById(R.id.progressBarWait);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(getRecyclerViewLayoutManager());
        registerForContextMenu(recyclerView);

        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(this::onRefresh);
        swipeLayout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorPrimaryDark);

        // initialize fast scrolling
        VerticalRecyclerViewFastScroller fastScroller = (VerticalRecyclerViewFastScroller) findViewById(R.id.fast_scroller);
        SectionTitleIndicator sectionTitleIndicator = (SectionTitleIndicator) findViewById(R.id.fast_scroller_section_title_indicator);
        mFastScrollWrapper = new FastScrollWrapper(recyclerView, fastScroller, sectionTitleIndicator);

        initOnCreate();

        colorPaletteService().subscribe(collapsingToolbar::setExpandedTitleColor, this::onError);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            if (mLastFmArtist != null) {
                showDialog(mLastFmArtist.getName(), Utils.fromHtml(mLastFmArtist.getBio().content));
            } else {
                Toast.makeText(getApplicationContext(), "loading artist info, please try again", Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void startWaiting() {
        progressWait.setVisibility(View.VISIBLE);
    }

    protected void stopWaiting() {
        progressWait.setVisibility(View.GONE);
    }

    protected void onSongMenuClicked(final Song song, final View anchor){
        createPopupMenu(song, anchor, item -> onMenuItemClick(item, song));
    }

    protected boolean onMenuItemClick(final MenuItem item, final Song song) {
        switch(item.getItemId()) {
            case R.id.download:
                AmpacheCache.INSTANCE.downloadSong(getApplicationContext(),song)
                .subscribe(aBoolean -> Toast.makeText(getApplicationContext(),R.string.download_song_ok,Toast.LENGTH_LONG).show(),
                        throwable -> Toast.makeText(getApplicationContext(),R.string.download_song_error,Toast.LENGTH_LONG).show());
                break;
            case R.id.delete_download:
                AmpacheCache.INSTANCE.deleteSongFromCache(getApplicationContext(), song);
                break;
            case R.id.menu_go_to_album:
                AmpacheApi.INSTANCE.getAlbumFromId(song.getAlbum().getId())
                        .subscribe(album -> goToAlbumActivity(album,null),this::onError);
                break;
            case R.id.menu_go_to_artist:
                AmpacheApi.INSTANCE.getArtistFromId(song.getArtist().getId())
                        .subscribe(artist -> goToArtistActivity(artist,null),this::onError);
                break;
            case R.id.menu_play_next:
                // play next
                PlayManager.INSTANCE.playNext(song);
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    private void createPopupMenu(Song song, View anchor, OnMenuItemClickListener clickListener) {
        mPopupMenu = new PopupMenu(this, anchor);
        mPopupMenu.getMenuInflater().inflate(R.menu.menu_song, mPopupMenu.getMenu());
        mPopupMenu.getMenu().findItem(R.id.menu_remove).setVisible(isMenuRemoveVisible());

        boolean isSongCached = AmpacheCache.INSTANCE.isSongInCache(getApplicationContext(), song);
        mPopupMenu.getMenu().findItem(R.id.delete_download).setVisible(isSongCached);
        mPopupMenu.getMenu().findItem(R.id.download).setVisible(!isSongCached);


        mPopupMenu.setOnMenuItemClickListener(clickListener);
        mPopupMenu.show();
    }

    protected boolean isMenuRemoveVisible() {
        return false;
    }
}
