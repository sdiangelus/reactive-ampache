/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.adapters;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.ui.adapters.dragdrop.ItemTouchHelperViewHolder;

/**
 * Created by antonio tari on 2016-07-21.
 */
class SongViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
    final TextView songName;
    final TextView songDuration;
    final TextView songNumber;
    final ImageView songImage;
    final ImageView dragView;
    final ImageButton removePlaylist;
    final ImageButton songMenuButton;
    final ImageButton addToPlaylistBtn;
    final View mainRowLayout;
    final CardView mainCardView;

    final int startColour;

    SongViewHolder(View v) {
        super(v);
        songName = (TextView) v.findViewById(R.id.songName);
        songDuration = (TextView) v.findViewById(R.id.songDuration);
        songNumber = (TextView) v.findViewById(R.id.songNumber);
        songImage = (ImageView) v.findViewById(R.id.songImage);
        dragView = (ImageView) v.findViewById(R.id.dragView);
        removePlaylist = (ImageButton) v.findViewById(R.id.removePlaylistBtn);
        addToPlaylistBtn = (ImageButton) v.findViewById(R.id.addToPlaylistBtn);
        songMenuButton = (ImageButton) v.findViewById(R.id.songMenuButton);
        mainRowLayout = v.findViewById(R.id.mainRowLayout);
        mainCardView  = (CardView) v.findViewById(R.id.mainCardView);

        // the original colour, need to store to come back to it after drop
        startColour = mainCardView.getContext().getResources().getColor(R.color.card_bg);
        //            addToPlaylistBtn.setOnCreateContextMenuListener(this);
//            addToPlaylistBtn.setOnClickListener(View::showContextMenu);
    }

    @Override
    public void onItemSelected() {
        mainCardView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        mainCardView.setBackgroundColor(startColour);
    }
}