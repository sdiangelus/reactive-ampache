/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.antoniotari.android.lastfm.LastFm;
import com.antoniotari.android.lastfm.LastFmArtist;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.models.Playlist;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.managers.PlayManager.SongCompletedListener;
import com.antoniotari.reactiveampacheapp.ui.adapters.AlbumsAdapter.OnAlbumClickListener;
import com.antoniotari.reactiveampacheapp.ui.adapters.ArtistsAdapter.OnArtistClickListener;
import com.antoniotari.reactiveampacheapp.ui.adapters.PlaylistsAdapter.OnPlaylistClickListener;
import com.antoniotari.reactiveampacheapp.utils.Utils;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.io.IOException;

import nl.changer.audiowife.AudioWife;
import rx.Observable;
import rx.Observable.OnSubscribe;
import rx.Subscriber;
import rx.Subscription;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subjects.Subject;

/**
 * Created by antonio tari on 2016-05-23.
 */
public abstract class BaseMusicActivity extends AppCompatActivity implements SongCompletedListener, OnAlbumClickListener,
                                                                             OnArtistClickListener, OnPlaylistClickListener {

    public static final String KEY_INTENT_LASTFM_ARTIST = "com.antoniotari.reactiveampacheapp.albumactivity.lastfm_artist";

    protected ImageButton mPlayBtn;
    protected ImageButton mStopBtn;
    protected ImageButton mPrevBtn;
    protected ImageButton mNextBtn;
    private ImageButton shuffleButton;

    protected SeekBar mSeekBar;
    protected TextView mElapsedTimeTextView;
    protected TextView mTotalTimeTextView;
    protected TextView mSongTextView;
    private AlertDialog mWaitDialog;

    private Subscription mSubscription;
    private Subject<Integer, Integer> mColorPaletteSubj = PublishSubject.create();

    protected abstract int getLayout();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());

        mPlayBtn = (ImageButton) findViewById(R.id.playButton);
        mStopBtn = (ImageButton) findViewById(R.id.stopButton);
        mPrevBtn = (ImageButton) findViewById(R.id.prevButton);
        mNextBtn = (ImageButton) findViewById(R.id.nextButton);
        shuffleButton = (ImageButton) findViewById(R.id.shuffleButton);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mElapsedTimeTextView = (TextView) findViewById(R.id.elapsedTextView);
        mTotalTimeTextView = (TextView) findViewById(R.id.totalTextView);
        mSongTextView = (TextView) findViewById(R.id.playerSongName);

        mNextBtn.setOnClickListener(view -> {
            PlayManager.INSTANCE.playNext();
            initMusicComponents();
        });
        mPrevBtn.setOnClickListener(view -> {
            PlayManager.INSTANCE.playPrevious();
            initMusicComponents();
        });

        logUser();
    }


    @Override
    protected void onResume() {
        super.onResume();
        initMusicComponents();
        mSubscription = PlayManager.INSTANCE.mSongSubject.subscribe(song -> initMusicComponents(), this::onError);
        PlayManager.INSTANCE.addSongCompletedListener(this);

        // initialize shuffle button
        toggleShuffle(PlayManager.INSTANCE.isShuffleOn());
        shuffleButton.setOnClickListener(v -> toggleShuffle(PlayManager.INSTANCE.shufflePlaylist()));

        AmpacheApi.INSTANCE.ping().subscribe(pingResponse -> {
            // response
        }, this::onError);
    }

    private void initMusicComponents() {
        AudioWife.getInstance().setPlayView(mPlayBtn);
        AudioWife.getInstance().setPauseView(mStopBtn);
        AudioWife.getInstance().setNextView(mNextBtn);
        AudioWife.getInstance().setPrevView(mPrevBtn);

        try {
            AudioWife.getInstance().setSeekBar(mSeekBar);
        } catch (Exception e) {
        }
        try {
            AudioWife.getInstance().setTotalTimeView(mTotalTimeTextView);
        } catch (Exception e) {
        }
        try {
            AudioWife.getInstance().setRuntimeView(mElapsedTimeTextView);
        } catch (Exception e) {
        }

        if (AudioWife.getInstance().getMediaPlayer() != null) {

            if (AudioWife.getInstance().getMediaPlayer().isPlaying()) {
                mPlayBtn.setVisibility(View.GONE);
                mStopBtn.setVisibility(View.VISIBLE);
            } else {
                mStopBtn.setVisibility(View.GONE);
                mPlayBtn.setVisibility(View.VISIBLE);
            }
        }

        mPlayBtn.setOnClickListener(v -> {
                try {
                    AudioWife.getInstance().play();
                }catch (Exception e) {
                    Crashlytics.logException(e);
                    Toast.makeText(getApplicationContext(),R.string.error_media_player, Toast.LENGTH_LONG).show();
                }
            });

        mStopBtn.setOnClickListener(v -> AudioWife.getInstance().pause());

        if (PlayManager.INSTANCE.getCurrentSong() != null) {
            Utils.setHtmlString(mSongTextView,
                    PlayManager.INSTANCE.getCurrentSong().getArtist().getName() + " - " + PlayManager.INSTANCE.getCurrentSong().getTitle());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSubscription != null && !mSubscription.isUnsubscribed()) {
            mSubscription.unsubscribe();
        }
        PlayManager.INSTANCE.removeSongCompletedListener(this);
    }

    protected Observable<Integer> colorPaletteService() {
        return mColorPaletteSubj;
    }

    protected Observable<LastFmArtist> artistInfoService(String artistName) {
        return AndroidObservable.bindActivity(this,
                Observable.create(new OnSubscribe<LastFmArtist>() {
                    @Override
                    public void call(final Subscriber<? super LastFmArtist> subscriber) {
                        try {
                            subscriber.onNext(LastFm.INSTANCE.getArtistImage(artistName));
                            subscriber.onCompleted();
                        } catch (Exception e) {
                            subscriber.onError(e);
                        }
                    }
                })).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    protected void loadArtistImage(LastFmArtist lastFmArtist, ImageView imageView, boolean isBlur) {
        bitmapService(lastFmArtist.getImages().getBiggestAvailable())
                .subscribe(bitmap -> {

                    Palette.from(bitmap).generate(p -> mColorPaletteSubj.onNext(p.getLightVibrantColor(Color.WHITE)));

                    if (isBlur) {
                        renderScriptBlur(RenderScript.create(getApplicationContext()), bitmap, 25);
                    }
                    imageView.setImageBitmap(bitmap);
                }, this::onError);
    }

    protected Observable<Bitmap> bitmapService(final String url) {
        return AndroidObservable.bindActivity(this,
                Observable.create(new OnSubscribe<Bitmap>() {
                    @Override
                    public void call(final Subscriber<? super Bitmap> subscriber) {
                        try {
                            Bitmap bitmap = Picasso.with(getApplicationContext()).load(url).get();
                            subscriber.onNext(bitmap);
                            subscriber.onCompleted();
                        } catch (IOException e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                    }
                })).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Uses RenderScript to Blur a Bitmap.
     *
     * @param rs         can use RenderScript.create(context). If blurring multiple times, pass the same instance of RenderScript.
     * @param bitmap     Your original Bitmap.
     * @param blurRadius The radius which defines the amount to blur the image. Range is 0 - 25 inclusive.
     */
    @TargetApi (17)
    public static void renderScriptBlur(RenderScript rs, Bitmap bitmap, float blurRadius) {
        final Allocation input = Allocation.createFromBitmap(rs, bitmap); //use this constructor for best performance,
        // because it uses USAGE_SHARED mode which reuses memory
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(blurRadius);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(bitmap);
    }

    public void onError(Throwable throwable) {
        Utils.onError(getApplicationContext(), throwable);
        if (Utils.is400Error(throwable)) {
            goToSplash();
        }
    }

    public void showDialog(String title, String text) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    // continue
                }).show();
    }

    public void showDialog(String title, Spanned text) {
        new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(text)
                .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    // continue
                }).show();
    }


    public void showWaitDialog() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.progress_circle, null);

        if(mWaitDialog == null) {
            mWaitDialog = new AlertDialog.Builder(this, R.style.WaitingDialog)
                    .setView(view).show();
        } else if(!mWaitDialog.isShowing()) {
            mWaitDialog.show();
        }
    }

    public void stopWaitDialog() {
        if(mWaitDialog!=null){
            mWaitDialog.dismiss();
        }
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        initMusicComponents();
    }

    @Override
    public void onAlbumClick(Album album, LastFmArtist lastFmArtist) {
        goToAlbumActivity(album, lastFmArtist);
    }

    protected void goToSplash() {
        Intent intent = new Intent(this, SplashActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    protected void goToAlbumActivity(Album album, LastFmArtist lastFmArtist) {
        Intent intent = new Intent(this, AlbumActivity.class);
        intent.putExtra(AlbumActivity.KEY_INTENT_ALBUM, album);
        try {
            intent.putExtra(KEY_INTENT_LASTFM_ARTIST, lastFmArtist);
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
        startActivity(intent);
    }

    protected void goToArtistActivity(Artist artist, LastFmArtist lastFmArtist) {
        Intent intent = new Intent(this, ArtistActivity.class);
        intent.putExtra(ArtistActivity.KEY_INTENT_ARTIST, artist);
        //AmpacheCache.INSTANCE.addArtistToCache(artist);
        try {
            intent.putExtra(KEY_INTENT_LASTFM_ARTIST, lastFmArtist);
        } catch (Exception e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }

    protected void goToPlaylistActivity(final Playlist playlist) {
        if(playlist.getItems() == 0){
            Toast.makeText(this,R.string.empty_playlist,Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, PlaylistActivity.class);
            intent.putExtra(PlaylistActivity.KEY_INTENT_PLAYLIST, playlist);
            startActivity(intent);
        }
    }

    protected void goToOfflinePlaylistActivity(final Playlist playlist) {
        Intent intent = new Intent(this, OfflinePlaylistActivity.class);
        intent.putExtra(PlaylistActivity.KEY_INTENT_PLAYLIST, playlist);
        startActivity(intent);
    }


    @Override
    public void onPlaylistClick(final Playlist playlist) {
        goToPlaylistActivity(playlist);
    }

    @Override
    public void onArtistClick(Artist artist, LastFmArtist lastFmArtist) {
        goToArtistActivity(artist, lastFmArtist);
    }

    private void logUser() {
        // You can call any combination of these three methods
//        Crashlytics.setUserIdentifier(AmpacheSession.INSTANCE.getAmpachePassword());
//        Crashlytics.setUserEmail(AmpacheSession.INSTANCE.getAmpacheUrl());
//        Crashlytics.setUserName(AmpacheSession.INSTANCE.getAmpacheUser());
    }

    private void toggleShuffle(final boolean isShuffleOn) {
        if (isShuffleOn) {
            shuffleButton.setImageResource(R.drawable.ic_shuffle);
        } else {
            shuffleButton.setImageResource(R.drawable.ic_shuffle_off);
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        if (Build.VERSION.SDK_INT < VERSION_CODES.N) {
            super.onSaveInstanceState(outState);
        }
    }
}
