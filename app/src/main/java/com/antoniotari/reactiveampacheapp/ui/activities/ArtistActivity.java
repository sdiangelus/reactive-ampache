/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView.LayoutManager;

import java.util.List;

import com.antoniotari.android.lastfm.LastFmArtist;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.models.Album;
import com.antoniotari.reactiveampache.models.Artist;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.ui.adapters.AlbumsAdapter;

import rx.Observable;
import rx.android.observables.AndroidObservable;

/**
 * Created by antonio tari on 2016-05-21.
 */
public class ArtistActivity extends BaseDetailActivity {

    public static final String KEY_INTENT_ARTIST = "com.antoniotari.reactiveampacheapp.artistactivity.artist";

    private Artist mArtist;
    private AlbumsAdapter mAlbumsAdapter;

    @Override
    protected void initOnCreate() {
        mArtist = getIntent().getParcelableExtra(KEY_INTENT_ARTIST); //AmpacheCache.INSTANCE.getArtist(getIntent().getStringExtra(KEY_INTENT_ARTIST)); //
        mLastFmArtist = getIntent().getParcelableExtra(KEY_INTENT_LASTFM_ARTIST);

        setDetailName(mArtist.getName());

        if (mLastFmArtist == null) {
            class ZipWrapper {
                LastFmArtist lastFmArtist;
                List<Album> albumList;

                public ZipWrapper(final List<Album> albumList, final LastFmArtist lastFmArtist) {
                    this.lastFmArtist = lastFmArtist;
                    this.albumList = albumList;
                }
            }

            // we can initialize lastFm artist in the adapter only when both calls are completed
            AndroidObservable.bindActivity(this,
                    Observable.zip(AmpacheApi.INSTANCE.getAlbumsFromArtist(mArtist.getId()),
                            artistInfoService(mArtist.getName()),
                            ZipWrapper::new))
                    .subscribe(zipWrapper -> {
                        Log.log("getAlbumsFromArtist returned ");
                                onArtist(zipWrapper.lastFmArtist);
                                initAdapter(zipWrapper.albumList);
                            }, this::onError);
        } else {
            onArtist(mLastFmArtist);
            AndroidObservable.bindActivity(this,
                    AmpacheApi.INSTANCE.getAlbumsFromArtist(mArtist.getId()))
                    .subscribe(this::initAdapter, this::onError);
        }
    }

    private void onArtist(LastFmArtist lastFmArtist) {
        mLastFmArtist = lastFmArtist;
        loadArtistImage(lastFmArtist, mToolbarImageView, false);
        loadArtistImage(lastFmArtist, mBackgroundImage, true);
    }

    @Override
    protected LayoutManager getRecyclerViewLayoutManager() {
        return new GridLayoutManager(this, getResources().getInteger(R.integer.column_num));
    }

    @Override
    protected void onRefresh() {
        AndroidObservable.bindActivity(this,
                AmpacheApi.INSTANCE.handshake()
                        .flatMap(handshakeResponse -> AmpacheApi.INSTANCE.getAlbumsFromArtist(mArtist.getId())))
                .subscribe(albumList -> {
                    initAdapter(albumList);
                    swipeLayout.setRefreshing(false);
                }, this::onError);
    }

    protected void initAdapter(List<Album> albumList) {
        stopWaiting();
        if (mAlbumsAdapter == null) {
            mAlbumsAdapter = new AlbumsAdapter(albumList);
            mAlbumsAdapter.setOnAlbumClickListener(this);
            mAlbumsAdapter.setLastFmArtist(mLastFmArtist);
            recyclerView.setAdapter(mAlbumsAdapter);
        } else {
            mAlbumsAdapter.setAlbums(albumList);
            mAlbumsAdapter.notifyDataSetChanged();
        }

        // once we have the album list determine if fast scroll is needed
        mFastScrollWrapper.determineSetFastScroll(albumList.size());
    }
}
