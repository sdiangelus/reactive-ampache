/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.ui.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampacheapp.BuildConfig;
import com.antoniotari.reactiveampacheapp.MainActivity;
import com.antoniotari.reactiveampacheapp.R;
import com.antoniotari.reactiveampacheapp.utils.Utils;

public class LoginActivity extends AppCompatActivity {

    private static final String KEY_PREFERENCES_URL = "com.antoniotari.reactiveapp.saved.url";
    private static final String KEY_PREFERENCES_USER = "com.antoniotari.reactiveapp.saved.username";

    // TODO replace with string in res R.string.url_demo_server, etc..
    private static final String URL_DEMO_SERVER = "http://play.dogmazic.net/";
    private static final String USERNAME_DEMO_SERVER = "powerdemo";
    private static final String PASSWORD_DEMO_SERVER = "demo";

    private AutoCompleteTextView mUsernameView;
    private AutoCompleteTextView mUrlView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private View mDemoSignInButton;
    private Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);
        mUrlView = (AutoCompleteTextView) findViewById(R.id.ampacheUrl);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mPasswordView = (EditText) findViewById(R.id.password);
        mDemoSignInButton = findViewById(R.id.demo_sign_in_button);

        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            });

        mDemoSignInButton.setOnClickListener(v -> attemptLogin(URL_DEMO_SERVER, USERNAME_DEMO_SERVER, PASSWORD_DEMO_SERVER));
        mEmailSignInButton.setOnClickListener(view -> attemptLogin());
        setupTestCredentials(findViewById(R.id.bannerImageView));
        prepulateFields();
    }

    private void setupTestCredentials(View dummyView) {
        if (BuildConfig.DEBUG) {
            dummyView.setOnLongClickListener(v -> {
                mUsernameView.setText(BuildConfig.AMPACHE_USERNAME);
                mUrlView.setText(BuildConfig.AMPACHE_URL_LOCAL);
                mPasswordView.setText(BuildConfig.AMPACHE_PASSWORD);
                return false;
            });
        }
    }

    private void attemptLogin() {
        // Store values at the time of the login attempt.
        String url = mUrlView.getText().toString();
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();
        attemptLogin(url, username, password);
    }

    private boolean isDemoServer(String url, String username) {
        return !TextUtils.isEmpty(url) && !TextUtils.isEmpty(username) &&
                url.equals(URL_DEMO_SERVER) && username.equals(USERNAME_DEMO_SERVER);
    }

    /**
     * Attempts to sign in or register the account specified by the login form. If there are form errors (invalid email, missing fields,
     * etc.), the errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(String url, String username, String password) {
        if(TextUtils.isEmpty(url) || TextUtils.isEmpty(username) || TextUtils.isEmpty(url)) {
            Toast.makeText(getApplicationContext(), R.string.login_values_required, Toast.LENGTH_LONG).show();
            return;
        }
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        String http = "http://";
        String https = "https://";
        if (!http.equals(url.substring(0, http.length())) &&
                !https.equals(url.substring(0, https.length()))) {
            url = (http + url).trim();
            mUrlView.setText(url);
        }
        final String urlCopy = url;

        showProgress(true);
        AmpacheApi.INSTANCE.initUser(url,username,password)
                .flatMap(aVoid -> AmpacheApi.INSTANCE.handshake())
                .subscribe(handshakeResponse -> {
                    // save the credentials for later re-populate the login form
                    if (!isDemoServer(urlCopy, username)) {
                        saveCredentials(urlCopy, username);
                    }
                    //stop the progress view
                    showProgress(false);
                    goToMain();
                }, throwable -> {
                    AmpacheApi.INSTANCE.resetCredentials();

                    int message;
                    if(throwable instanceof IllegalArgumentException){
                        message = R.string.something_wrong_url;
                    } else {
                        message = R.string.something_wrong;
                    }
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    Utils.onError(this, throwable);
                    showProgress(false);
                },()-> showProgress(false));
    }

    private void goToMain() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi (Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * pre-populate fields with last login
     */
    private void prepulateFields() {
        String username = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(KEY_PREFERENCES_USER, null);
        String url = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(KEY_PREFERENCES_URL, null);
        if (!TextUtils.isEmpty(username)
                && !TextUtils.isEmpty(url)) {
            mUrlView.setText(url);
            mUsernameView.setText(username);
        }
    }

    /**
     * saves
     * @param url
     * and
     * @param username
     * for later pre-populate the login form fields
     *
     */
    private void saveCredentials(final String url, final String username) {
        Editor editor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
        editor.putString(KEY_PREFERENCES_URL,url);
        editor.putString(KEY_PREFERENCES_USER,username);
        editor.apply();
    }
}
